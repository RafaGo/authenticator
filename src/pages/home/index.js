import React, { useState } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, Modal, TextInput } from "react-native";
import Slider from "@react-native-community/slider";
import { ModalPassword } from "../../components/modal";

let charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

export  function Home() {
    const [size, setSize] = useState(10);
    const [passwordValue, setPasswordValue] = useState("");
    const [initials, setInitials] = useState("");
    const [modalVisible, setModalVisible] = useState(false);

    function generatePassword() {
        if (initials.length < 4) {
            alert("Insira pelo menos 4 iniciais.");
            return;
        }

        const initialsToUse = initials.slice(0, 4);
        let password = initialsToUse;
        const combinedCharset = charset;
        const remainingPasswordLength = size - initialsToUse.length;
        for (let i = 0, n = combinedCharset.length; i < remainingPasswordLength; i++) {
            password += combinedCharset.charAt(Math.floor(Math.random() * n));
        }
        setPasswordValue(password);
        setModalVisible(true);
        setInitials('')
    }

    return (
        <View style={styles.container}>
            <Image source={require("../../assets/logo.png")} style={styles.logo} />

            <Text style={styles.title}>{size} caracteres</Text>

            <TextInput
                style={styles.input}
                placeholder="Digite seu nome"
                value={initials}
                onChangeText={(text) => setInitials(text)}
            />

            <View style={styles.area}>
                <Slider
                    style={{ height: 150 }}
                    minimumValue={6}
                    maximumValue={20}
                    maximumTrackTintColor="#ff0000"
                    minimumTrackTintColor="#f3f"
                    thumbTintColor="#392de9"
                    value={size}
                    onValueChange={(value) => setSize(parseInt(value.toFixed(0)))}
                />
            </View>

            <TouchableOpacity style={styles.button} onPress={generatePassword}>
                <Text style={styles.buttonText}>Gerar Senha</Text>
            </TouchableOpacity>

            <Modal visible={modalVisible} animationType="fade" transparent={true}>
                <ModalPassword password={passwordValue} handleClose={() => setModalVisible(false)} />
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F3F3ff",
        justifyContent: "center",
        alignItems: "center",
    },
    logo: {
        marginBottom: 60,
    },
    area: {
        marginTop: 14,
        marginBottom: 14,
        width: "80%",
        backgroundColor: "#FFF",
        borderRadius: 8,
        padding: 6,
    },
    button: {
        backgroundColor: "#392de9",
        width: "80%",
        height: 100,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonText: {
        color: "#fff",
        fontSize: 50,
        fontWeight: "bold",
    },
    title: {
        fontSize: 50,
        fontWeight: "bold",
    },
    input: {
        fontSize: 30,
        width: "80%",
        height: 60,
        borderWidth: 1,
        borderColor: "#ccc",
        borderRadius: 8,
        marginBottom: 20,
        paddingHorizontal: 10,
    },
});
