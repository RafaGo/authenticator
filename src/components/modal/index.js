import { View, Text, StyleSheet, TouchableOpacity, Pressable, Alert } from "react-native";
import useStorage from "../../hooks/useStorage";
import Clipboard from '@react-native-clipboard/clipboard';

export function ModalPassword({ password, handleClose }) {

    const { saveItem } = useStorage();

    async function handleCopyPassword() {
        await Clipboard.setString(password); // Correção: use Clipboard.setString em vez de Clipboard.setStrings
        await saveItem('@pass', password); // Salva a senha após defini-la na área de transferência

        Alert.alert("Senha copiada com sucesso");
        handleClose();
    }

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Text style={styles.title}>Senha Gerada</Text>

                <Pressable style={styles.InnerPassword} onLongPress={handleCopyPassword}>
                    <Text style={styles.text}>{password}</Text>
                </Pressable>

                <View style={styles.buttonArea}>
                    <TouchableOpacity style={styles.button} onPress={handleClose}>
                        <Text style={styles.buttonText}>Voltar</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.button, styles.buttonSave]} onPress={handleCopyPassword}>
                        <Text style={styles.buttonTextSave}>Salvar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "rgba(24,24,24,0.6)",
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        backgroundColor: "#fff",
        width: '85%',
        paddingTop: 24,
        paddingBottom: 24,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
    },
    title: {
        fontSize: 40,
        fontWeight: "bold",
        color: "#000",
        marginBottom: 24,
    },
    InnerPassword: {
        backgroundColor: "#0e0e0e",
        width: '90%',
        padding: 30,
        borderRadius: 8,
    },
    text: {
        color: "#fff",
        textAlign: "center",
        fontSize: 30
    },
    buttonArea: {
        flexDirection: 'row',
        width: '90%',
        marginTop: 8,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    button: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 14,
        marginTop: 14,
        padding: 8
    },
    buttonSave: {
        backgroundColor: "#392de9",
        borderRadius: 8,
    },
    buttonText: {
        fontSize: 40
    },
    buttonTextSave: {
        color: '#fff',
        fontWeight: "bold",
        fontSize: 40
    }
})

