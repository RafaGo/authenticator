import { Home } from "./pages/home";
import { Passwords } from "./pages/passwords"; 
import { createBottomTabNavigator}  from '@react-navigation/bottom-tabs'
import { StyleSheet } from "react-native";


const Tab = createBottomTabNavigator();

export function Routes() {
    return (
        <Tab.Navigator>
        <Tab.Screen 
            name="HOME"
            component={Home}
            options={{
                tabBarShowLabel:true,
                headerShown: false,
            }}
        />
        <Tab.Screen
            name="MINHAS SENHAS"
            component={Passwords}
            options={{ 
                tabBarShowLabel:true,
                headerShown: false,
            }}
        />
    </Tab.Navigator>
)}    